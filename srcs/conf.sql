create user '${USER}'@'proxy' identified by '${PASS}';
create user 'root'@'%' identified  by 'root';
create database aor;

use aor;
create table people (
    i int4 auto_increment,
    number int8,
    fio varchar(200),
    birth date,
    address varchar(300),
    sex bit,
    operator varchar(320),
    region_name varchar(160),
    timezone int2,
    sold int2,
    call_status int2,
    region_number int2,
    file_name varchar(100),
    primary key(i)
                    );

GRANT ALL PRIVILEGES ON aor.people TO 'root'@'%';
GRANT ALL PRIVILEGES ON aor.people TO '${USER}'@'proxy';
FLUSH PRIVILEGES;
