FROM debian:buster
WORKDIR /
ARG PASS=default
ARG USER=default
ENV PASS=$PASS
ENV USER=$USER
RUN apt-get update && apt-get install -y mariadb-server gettext
COPY srcs/50-server.cnf /etc/mysql/mariadb.conf.d/
RUN chmod  +x /etc/mysql/mariadb.conf.d/50-server.cnf
ADD srcs/50-client.cnf /etc/mysql/mariadb.conf.d/
RUN chmod +x /etc/mysql/mariadb.conf.d/50-client.cnf
COPY srcs/conf.sql .
ADD srcs/run_service.sh .
RUN chmod +x run_service.sh
EXPOSE 3306
RUN service mysql start && envsubst < conf.sql | mysql && service mysql stop
CMD ./run_service.sh